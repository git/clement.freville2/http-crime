CC := gcc
CFLAGS := -std=gnu11 -Wall -Wextra -pedantic -g
LDLIBS := -lssl -lcrypto -lz

all: server client

server: server.c
	$(CC) $(CFLAGS) $^ $(LDLIBS) -o $@

client: client.c
	$(CC) $(CFLAGS) $^ $(LDLIBS) -o $@

format:
	clang-format -i *.c

clean:
	rm client server
