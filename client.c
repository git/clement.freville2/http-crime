#include <netdb.h>
#include <openssl/err.h>
#include <openssl/ssl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <zlib.h>

#define BUF_LEN 1024
#define SECRET "eiph0aChThu17ah9rua4Naiv4"

char *unscramble(char *s) {
    for (char *str = s + 1; *str != 0; str += 2) {
        *s++ = *str;
    }
    *s = '\0';
    return s;
}

void open_connection(const char *hostname, const char *port, const char *msg) {
    int sfd, s;
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;

    s = getaddrinfo(hostname, port, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1) {
            continue;
        }

        if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1) {
            break; // Success
        }

        close(sfd);
    }
    freeaddrinfo(result);

    if (rp == NULL) {
        fprintf(stderr, "Could not connect\n");
        exit(EXIT_FAILURE);
    }

    SSL_library_init();
    OpenSSL_add_all_algorithms();
    SSL_CTX *ctx = SSL_CTX_new(TLS_method());
    SSL_CTX_set_security_level(ctx, 1);
    SSL_CTX_clear_options(ctx, SSL_OP_NO_COMPRESSION);
    SSL *ssl = SSL_new(ctx);
    SSL_set_fd(ssl, sfd);

    SSL_connect(ssl);

    STACK_OF(SSL_COMP) *st = SSL_COMP_get_compression_methods();
    int has_compression = sk_SSL_COMP_num(st);
    if (st == NULL) {
        fprintf(stderr, "Cannot list compression methods\n");
    } else if (sk_SSL_COMP_num(st) == 0) {
        fprintf(stderr, "No compression methods enabled\n");
    }

    char flag[sizeof(SECRET)];
    strcpy(flag, SECRET);
    unscramble(flag);

    char buf[BUF_LEN] = {0};
    strcat(buf, "GET /");
    strncat(buf, msg, BUF_LEN / 2);
    strcat(buf, " HTTP/1.1\r\nHost: iut.local\r\n");
    strcat(buf, "Connection: close\r\n");
    strcat(buf, "Cookie: flag=");
    strcat(buf, flag);
    strcat(buf, "\r\n");
    SSL_write(ssl, buf, strlen(buf));

    if (has_compression) {
        uLong ucompSize = strlen(buf);
        // Use a compression level of 9
        uLong compSize = compressBound(ucompSize);
        Bytef *comp = malloc(compSize);
        compress(comp, &compSize, (unsigned char *)buf, ucompSize);
        free(comp);
        printf("Sent %lu data bytes\n", compSize);
    } else {
        printf("Sent %lu data bytes\n", strlen(buf));
    }

    ssize_t nb_read = read(sfd, buf, BUF_LEN - 1);
    buf[nb_read] = '\0';
    printf("%s\n", buf);

    SSL_free(ssl);
    SSL_CTX_free(ctx);
}

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <hostname> <port> <message>\n", argv[0]);
        return 1;
    }
    open_connection(argv[1], argv[2], argv[3]);
    return 0;
}
